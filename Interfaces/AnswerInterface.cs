﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Models
{
    public class AnswerInterface : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }

        public Answer Answer { get; }
        public bool IsChecked { get; set; }

        private string index;
        public string Index
        {
            get { return index; }
            set { index = value; OnPropertyChanged("Presentation"); }
        }

        public string Presentation
        {
            get
            {
                return Index + " " + Answer.Content;
            }
        }

        public AnswerInterface(Answer answer, char index)
        {
            Answer = answer;
            IsChecked = false;
            Index = index + ")";
        }
    }
}
