﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace surveyapp.Models
{
    public class QuestionInterface : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }

        public Question Question { get; }
        private ObservableCollection<AnswerInterface> answers;
        public ObservableCollection<AnswerInterface> Answers
        {
            get { return answers; }
            set { answers = value; OnPropertyChanged("Answers"); }
        }

        public Visibility NoAnswersVisibility
        {
            get
            {
                if (Answers == null || Answers.Count == 0)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        public Visibility ChartVisibility
        {
            get
            {
                if (Answers == null || Answers.Count == 0)
                    return Visibility.Collapsed;

                return Visibility.Visible;
            }
        }

        public string Index { get; set; }
        public string Presentation
        {
            get { return Index + " " + Question.Content; }
        }

        public Visibility ClosedQuestionVisibility
        {
            get
            {
                if (Question.Type == 0)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }
        public Visibility OpenedQuestionVisibility
        {
            get
            {
                if (Question.Type == 1)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }
        public Visibility RangedQuestionVisibility
        {
            get
            {
                if (Question.Type == 2)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }
        public string OpenedAnswer { get; set; }

        private int sliderValue;
        public int SliderValue
        {
            get { return sliderValue; }
            set { sliderValue = value; OnPropertyChanged("SliderLabel"); }
        }

        public string SliderLabel
        {
            get
            {
                string label;
                if (SliderValue == 0)
                    label = "Zdecydowanie się nie zgadzam";
                else if (SliderValue == 1)
                    label = "Nie zgadzam się";
                else if (SliderValue == 2)
                    label = "Nie wiem";
                else if (SliderValue == 3)
                    label = "Zgadzam się";
                else
                    label = "Zdecydowanie się zgadzam";

                return label;
            }
        }

        public QuestionInterface(Question question, ObservableCollection<AnswerInterface> answers, int index)
        {
            Question = question;
            Answers = answers;
            SliderValue = 2;
            Index = "#" + index;
        }
    }
}
