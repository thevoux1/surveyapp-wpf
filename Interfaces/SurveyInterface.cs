﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace surveyapp.Interfaces
{
    public class SurveyInterface
    {
        public Survey Survey { get; set; }
        public ObservableCollection<QuestionInterface> Questions { get; }
        public string Privacy
        {
            get
            {
                if (Survey.AccessKey == null)
                    return "publiczna";

                return "prywatna (klucz dostępu: " + Survey.AccessKey + ")";
            }
        }
        public Visibility EndSurveyVisibility
        {
            get
            {
                if (Survey.EndDate <= DateTime.Now)
                    return Visibility.Collapsed;

                return Visibility.Visible;
            }
        }

        public DateTime TimeNow { get => DateTime.Now; }

        public int NumberOfParticipants
        {
            get
            {
                if (Questions[0].Answers == null || Questions[0].Answers.Count == 0)
                    return 0;

                int number = 0;
                foreach (AnswerInterface answer in Questions[0].Answers)
                    number += answer.Answer.Selections;

                return number;
            }
        }

        public SurveyInterface(Survey survey, ObservableCollection<QuestionInterface> questions)
        {
            Survey = survey;
            Questions = questions;
        }
    }
}
