﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace surveyapp.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy ExportResultsWindow.xaml
    /// </summary>
    public partial class ExportResultsWindow : Window
    {
        public ExportResultsWindow()
        {
            InitializeComponent();
            string[] extensions = { ".png", ".jpg", ".bmp" };
            FileExtension_CB.ItemsSource = extensions;

            FileName_TB.Focus();
            FileName_TB.Select(0, 0);
        }

        private void ChooseDirectory_BT_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog openFileDialog = new CommonOpenFileDialog();
            openFileDialog.IsFolderPicker = true;
            openFileDialog.EnsureFileExists = true;
            openFileDialog.DefaultDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if(openFileDialog.ShowDialog() == CommonFileDialogResult.Ok)
                Directory_TB.Text = openFileDialog.FileName;

            Focus();
        }

        private void ExportFile_BT_Click(object sender, RoutedEventArgs e)
        {
            string fileName = FileName_TB.Text;

            if (fileName.Trim() == "")
            {
                MessageBox.Show("Nazwa pliku nie może być pusta", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (fileName.IndexOfAny(new char[] { '\\', '/', ':', '*', '?', '"', '<', '>', '|' }) != -1)
            {
                MessageBox.Show("Nazwa pliku zawiera niedozwolone znaki:\n\\ / : * ? \" < > |", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string directoryPath = Directory_TB.Text;

            if (!Directory.Exists(directoryPath))
            {
                MessageBox.Show("Folder docelowy nie został wybrany lub nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string filePath = directoryPath + "\\" + fileName + FileExtension_CB.SelectedItem;

            if(File.Exists(filePath))
            {
                MessageBoxResult result = MessageBox.Show("W folderze docelowym istnieje już plik o wybranej nazwie. Kontynowanie spowoduje jego nadpisanie. Czy kontynuować?", "Błąd", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                if(result != MessageBoxResult.Yes)
                    return;
            }

            SnapShotPNG(MainWindow.Instance.MainFrame, filePath, 1);
            Close();

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                Arguments = '"' + directoryPath + '"',
                FileName = "explorer.exe"
            };

            Process.Start(startInfo);
        }

        private void SnapShotPNG(UIElement source, string destination, int zoom)
        {
            try
            {
                double actualHeight = source.RenderSize.Height;
                double actualWidth = source.RenderSize.Width;

                double renderHeight = actualHeight * zoom;
                double renderWidth = actualWidth * zoom;

                RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)renderWidth, (int)renderHeight, 96, 96, PixelFormats.Pbgra32);
                VisualBrush sourceBrush = new VisualBrush(source);

                DrawingVisual drawingVisual = new DrawingVisual();
                DrawingContext drawingContext = drawingVisual.RenderOpen();

                using (drawingContext)
                {
                    drawingContext.PushTransform(new ScaleTransform(zoom, zoom));
                    drawingContext.DrawRectangle(sourceBrush, null, new Rect(new Point(0, 0), new Point(actualWidth, actualHeight)));
                }
                renderTarget.Render(drawingVisual);

                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(renderTarget));
                using (FileStream stream = new FileStream(destination, FileMode.Create, FileAccess.Write))
                {
                    encoder.Save(stream);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void CatchEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                ExportFile_BT_Click(sender, e);
        }
    }
}
