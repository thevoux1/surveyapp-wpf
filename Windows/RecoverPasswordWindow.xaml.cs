﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace surveyapp.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy RecoverPasswordWindow.xaml
    /// </summary>
    public partial class RecoverPasswordWindow : Window
    {
        public RecoverPasswordWindow()
        {
            InitializeComponent();
            Email_TB.Focus();
            Email_TB.Select(0, 0);
        }

        private void RecoverPassword_BT_Click(object sender, RoutedEventArgs e)
        {
            string email = Email_TB.Text.Trim();

            if(email == "")
            {
                MessageBox.Show("Email nie może być pusty", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                MailAddress mail = new MailAddress(email);
            }
            catch (FormatException)
            {
                MessageBox.Show("Nieprawidłowy format adresu e-mail", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ObservableCollection<User> users = Database.GetDatabase().Query<User>("from User where Email = '" + email + "'");
            if(users.Count < 1)
            {
                MessageBox.Show("Żadne konto nie jest powiązane z tym adresem email", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SendEmail(users[0]);
            Close();
            MessageBox.Show("Wiadomość e-mail z przypomnieniem hasła została wysłana", "Przypomnienie hasła", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void SendEmail(User user)
        {
            string recipent = user.Email;
            string subject = "Zapomniane hasło";
            string body = "Witaj, " + user.Login + "!\n\nHasło do Twojego konta to " + user.Password + "\nZapisz je sobie w bezpiecznym miejscu.\n\nPozdrawiamy,\nZespół SurveyApp";

            EmailSender.GetEmailSender().Send(recipent, subject, body);
        }

        private void CatchEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                RecoverPassword_BT_Click(sender, e);
        }
    }
}
