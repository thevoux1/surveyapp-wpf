﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace surveyapp.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public User NewUser { get; set; }

        public RegisterWindow()
        {
            InitializeComponent();
            Login_TB.Focus();
            Login_TB.Select(0, 0);
        }

        private void Register_BT_Click(object sender, RoutedEventArgs e)
        {
            string login = Login_TB.Text.Trim();
            string email = Email_TB.Text.Trim();
            string password = Password_TB.Password;
            string reapetPassword = RepeatPassword_TB.Password;

            if(login == "")
            {
                MessageBox.Show("Login nie może być pusty", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ObservableCollection<User> users = Database.GetDatabase().Query<User>("from User where Login = '" + login + "'");
            if(users.Count > 0)
            {
                MessageBox.Show("Użytkownik o takiej nazwie już istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (email == "")
            {
                MessageBox.Show("Email nie może być pusty", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                MailAddress mail = new MailAddress(email);
            }
            catch (FormatException)
            {
                MessageBox.Show("Nieprawidłowy format adresu e-mail", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            users = Database.GetDatabase().Query<User>("from User where Email = '" + email + "'");
            if (users.Count > 0)
            {
                MessageBox.Show("Użytkownik o takim adresie e-mail już istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(password.Trim() == "")
            {
                MessageBox.Show("Hasło nie może być puste", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (password.Length < 8)
            {
                MessageBox.Show("Hasło musi mieć co najmniej 8 znaków", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(password != reapetPassword)
            {
                MessageBox.Show("Hasła nie są ze sobą zgodne", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            NewUser = new User()
            {
                Login = login,
                Email = email,
                Password = password
            };

            SendEmail(email, login);
            DialogResult = true;
            Close();
        }

        public void SendEmail(string recipent, string userName)
        {
            string subject = "Witamy w aplikacji SurveyApp!";
            string body = "Witaj, " + userName + "!\n\nCieszymy się, że dołączyłeś/aś do naszej społeczności oraz życzmy miłego ankietowania!\n\nPozdrawiamy,\nZespół SurveyApp";

            EmailSender.GetEmailSender().Send(recipent, subject, body);
        }

        private void RecoverPassword_BT_Click(object sender, RoutedEventArgs e)
        {
            Close();
            RecoverPasswordWindow recoverPasswordWindow = new RecoverPasswordWindow();
            recoverPasswordWindow.ShowDialog();
        }

        private void CatchEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                Register_BT_Click(sender, e);
        }
    }
}
