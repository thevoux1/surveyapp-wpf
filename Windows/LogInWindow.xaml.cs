﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace surveyapp.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy LogInWindow.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {
        public User LoggedUser { get; set; }

        public LogInWindow()
        {
            InitializeComponent();
            Login_TB.Focus();
            Login_TB.Select(0, 0);
        }

        private void LogIn_BT_Click(object sender, RoutedEventArgs e)
        {
            string login = Login_TB.Text.Trim();
            string password = Password_TB.Password;

            if (login == "")
            {
                MessageBox.Show("Uzupełnij login", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (password.Trim() == "")
            {
                MessageBox.Show("Uzupełnij hasło", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ObservableCollection<User> users = Database.GetDatabase().Query<User>("from User where Login = '" + login + "'");
            if(users.Count == 0)
            {
                MessageBox.Show("Nieprawidłowy login", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(users[0].Password != password)
            {
                MessageBox.Show("Nieprawidłowe hasło", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            LoggedUser = users[0];
            DialogResult = true;
            Close();
        }

        private void RecoverPassword_BT_Click(object sender, RoutedEventArgs e)
        {
            Close();
            RecoverPasswordWindow recoverPasswordWindow = new RecoverPasswordWindow();
            recoverPasswordWindow.ShowDialog();
        }

        private void CatchEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                LogIn_BT_Click(sender, e);
        }
    }
}
