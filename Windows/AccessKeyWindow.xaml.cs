﻿using surveyapp.Interfaces;
using surveyapp.Models;
using surveyapp.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace surveyapp.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy AccessKeyWindow.xaml
    /// </summary>
    public partial class AccessKeyWindow : Window
    {
        public AccessKeyWindow()
        {
            InitializeComponent();
            AccessKey_TB.Focus();
            AccessKey_TB.Select(0, 0);
        }

        private void StartSurvey_BT_Click(object sender, RoutedEventArgs e)
        {
            string accessKey = AccessKey_TB.Text.Trim();

            if(accessKey == "")
            {
                MessageBox.Show("Kod dostępu nie może być pusty", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ObservableCollection<Survey> surveys = Database.GetDatabase().Query<Survey>("from Survey where AccessKey = '" + accessKey + "'");
            if(surveys.Count < 1)
            {
                MessageBox.Show("Błędny kod dostępu", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Survey survey = surveys[0];

            if(survey.User == null)
            {
                MessageBox.Show("Ankieta została usunięta przez autora", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(survey.StartDate > DateTime.Now)
            {
                MessageBox.Show("Ankieta jeszcze się nie rozpoczęła", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (survey.EndDate < DateTime.Now)
            {
                MessageBox.Show("Ankieta już się zakończyła", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (MainWindow.Instance.WarnUser() != MessageBoxResult.Yes)
                return;

            SurveyInterface surveyInterface = new SurveyInterface(survey, PublicSurveysPage.GetQuestionInterfaces(survey.ID));
            MainWindow.Instance.MainFrame.Content = new CompleteSurveyPage(surveyInterface);
            MainWindow.Instance.MainFrame_SV.ScrollToHome();
            Close();
        }

        private void CatchEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                StartSurvey_BT_Click(sender, e);
        }
    }
}
