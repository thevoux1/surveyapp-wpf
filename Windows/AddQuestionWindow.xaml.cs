﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace surveyapp.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy AddQuestion.xaml
    /// </summary>
    public partial class AddQuestionWindow : Window
    {
        public QuestionInterface CurrentQuestion { get; set; }
        private char indexCounter = 'a';

        public AddQuestionWindow(QuestionInterface question, bool isEdit)  
        {
            InitializeComponent();
            CurrentQuestion = question;
            DataContext = CurrentQuestion;

            if(isEdit)
            {
                Title = "Edytuj pytanie";
                AddQuestion_BT.Content = "Edytuj pytanie"; ;
            }

            Question_TB.Focus();
            Question_TB.Select(0, 0);
        }

        private void QuestionType_CB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Answers_LB == null)
                return;

            if(QuestionType_CB.SelectedIndex == 0)
            {
                if(CurrentQuestion.Answers == null || CurrentQuestion.Answers.Count < 1)
                    EmptyAnswers_LB.Visibility = Visibility.Visible;
                else
                    EmptyAnswers_LB.Visibility = Visibility.Collapsed;

                Answers_Grid.Visibility = Visibility.Visible;
                Answers_LB.Visibility = Visibility.Visible;
                AddAnswer_BT.Visibility = Visibility.Visible;
            }
            else
            {
                Answers_Grid.Visibility = Visibility.Collapsed;
                AddAnswer_BT.Visibility = Visibility.Collapsed;
            }
        }

        private void AddAnswer_BT_Click(object sender, RoutedEventArgs e)
        {
            Answer newAnswer = new Answer()
            {
                Question = CurrentQuestion.Question,
                Content = ""
            };

            AnswerInterface newAnswerInterface = new AnswerInterface(newAnswer, indexCounter);
            AddAnswerWindow addAnswerWindow = new AddAnswerWindow(newAnswerInterface, false);
            addAnswerWindow.ShowDialog();
            
            if(addAnswerWindow.DialogResult == true)
            {
                if(CurrentQuestion.Answers == null)
                {
                    CurrentQuestion.Answers = new ObservableCollection<AnswerInterface>();
                    EmptyAnswers_LB.Visibility = Visibility.Collapsed;
                }
                else if(CurrentQuestion.Answers.Count == 0)
                    EmptyAnswers_LB.Visibility = Visibility.Collapsed;

                CurrentQuestion.Answers.Add(addAnswerWindow.CurrentAnswer);
                indexCounter++;
            }
        }

        private void Remove_Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AnswerInterface answer = (AnswerInterface)(sender as Image).Tag;
            CurrentQuestion.Answers.Remove(answer);

            if (CurrentQuestion.Answers.Count == 0)
                EmptyAnswers_LB.Visibility = Visibility.Visible;

            indexCounter = 'a';
            foreach (AnswerInterface answeri in CurrentQuestion.Answers)
                answeri.Index = indexCounter++ + ")";

            Height -= 20;
        }

        private void Edit_Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AnswerInterface answer = (AnswerInterface)(sender as Image).Tag;
            AddAnswerWindow addAnswerWindow = new AddAnswerWindow(answer, true);
            addAnswerWindow.ShowDialog();
            
            if(addAnswerWindow.DialogResult == true)
            {
                answer = addAnswerWindow.CurrentAnswer;
                answer.OnPropertyChanged("Presentation");
            }
        }

        private void AddQuestion_BT_Click(object sender, RoutedEventArgs e)
        {
            if(Question_TB.Text.Trim() == "")
            {
                MessageBox.Show("Treść pytania nie może być pusta!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(CurrentQuestion.Question.Type == 0 && (CurrentQuestion.Answers == null || CurrentQuestion.Answers.Count < 2))
            {
                MessageBox.Show("Pytanie zamknięte musi mieć co najmniej dwie odpowiedzi!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DialogResult = true;
            Close();
        }

        private void CatchEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddQuestion_BT_Click(sender, e);
        }
    }
}
