﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace surveyapp.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy AddAnswerWindow.xaml
    /// </summary>
    public partial class AddAnswerWindow : Window
    {
        public AnswerInterface CurrentAnswer { get; set; }

        public AddAnswerWindow(AnswerInterface answer, bool isEdit)
        {
            InitializeComponent();
            CurrentAnswer = answer;
            DataContext = CurrentAnswer;

            if(isEdit)
            {
                Title = "Edytuj odpowiedź";
                AddAnswer_BT.Content = "Edytuj odpowiedź";
            }

            Answer_TB.Focus();
            Answer_TB.Select(0, 0);
        }

        private void AddAnswer_BT_Click(object sender, RoutedEventArgs e)
        {
            if(Answer_TB.Text.Trim() == "")
            {
                MessageBox.Show("Treść odpowiedzi nie może być pusta!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DialogResult = true;
            Close();
        }

        private void CatchEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddAnswer_BT_Click(sender, e);
        }
    }
}
