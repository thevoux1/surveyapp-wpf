﻿using surveyapp.Interfaces;
using surveyapp.Models;
using surveyapp.Pages;
using surveyapp.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace surveyapp
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private PublicSurveysPage publicSurveysPage;
        private CreateSurveyPage createSurveyPage;
        public MySurveysPage mySurveysPage;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }

        public static MainWindow Instance { get; set; }
        private bool isLeftSliderExpanded = false;
        private User currentUser { get; set; }
        public string UserWelcome
        {
            get
            {
                if (currentUser == null)
                    return "";

                return "Witaj, " + currentUser.Login;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            Instance = this;
            DataContext = this;
            publicSurveysPage = new PublicSurveysPage();
            UpdatePublicSurveys();
        }

        public void UpdatePublicSurveys()
        {
            ObservableCollection<Survey> publicSurveys = Database.GetDatabase().Query<Survey>("from Survey where User is not null and AccessKey is null");
            publicSurveys = publicSurveys.Where(x => x.StartDate <= DateTime.Now && x.EndDate > DateTime.Now).ToObservableCollection();
            publicSurveysPage.UpdatePublicSurveys(publicSurveys);
            MainFrame.Content = publicSurveysPage;
        }

        private void Logo_Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DoubleAnimation sliderAnim = new DoubleAnimation();
            DoubleAnimation logoAnim = new DoubleAnimation();

            if (isLeftSliderExpanded)
            {
                if (LeftSlider_TranslateTransform.X != -250)
                    return;

                isLeftSliderExpanded = false;
                sliderAnim.From = -250;
                sliderAnim.To = 0;
                sliderAnim.AccelerationRatio = 0.5;

                logoAnim.From = -90;
                logoAnim.To = 0;
                sliderAnim.DecelerationRatio = 0.5;
                Logo_IMG.ToolTip = "Ukryj menu";
            }
            else
            {
                if (LeftSlider_TranslateTransform.X != 0)
                    return;

                isLeftSliderExpanded = true;
                sliderAnim.From = 0;
                sliderAnim.To = -250;
                sliderAnim.DecelerationRatio = 0.5;

                logoAnim.From = 0;
                logoAnim.To = -90;
                sliderAnim.AccelerationRatio = 0.5;
                Logo_IMG.ToolTip = "Pokaż menu";
            }

            LeftSlider_Border.RenderTransform.BeginAnimation(TranslateTransform.XProperty, sliderAnim);
            Logo_IMG.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, logoAnim);
        }

        public void PublicSurveys_BT_Click(object sender, RoutedEventArgs e)
        {
            if (WarnUser() != MessageBoxResult.Yes)
                return;

            UpdatePublicSurveys();
            MainFrame_SV.ScrollToHome();
        }

        private void AccessKey_BT_Click(object sender, RoutedEventArgs e)
        {
            AccessKeyWindow accessKeyWindow = new AccessKeyWindow();
            accessKeyWindow.ShowDialog();
        }

        private void LogIn_TB_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            LogInWindow logInWindow = new LogInWindow();
            logInWindow.ShowDialog();
            if (logInWindow.DialogResult == true)
            {
                currentUser = logInWindow.LoggedUser;
                OnPropertyChanged("UserWelcome");
                Unlogged_Header.Visibility = Visibility.Collapsed;
                Logged_Header.Visibility = Visibility.Visible;
                MySurveys_BT.Visibility = Visibility.Visible;
                MessageBox.Show("Pomyślnie zalogowano, witamy w aplikacji", "Zalogowano", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Register_TB_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RegisterWindow registerWindow = new RegisterWindow();
            registerWindow.ShowDialog();
            if (registerWindow.DialogResult == true)
            {
                Database.GetDatabase().AddData<User>(registerWindow.NewUser);
                MessageBox.Show("Pomyślnie utworzono nowe konto. Zaloguj się, aby z niego korzystać.", "Utworzono konto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void LogOut_TB_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (MainFrame.Content.GetType() == typeof(CreateSurveyPage))
            {
                if (MessageBox.Show("Opuszczenie kreatora poskutuje utratą ankiety, kontynuować?", "Uwaga", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
            }

            currentUser = null;
            Unlogged_Header.Visibility = Visibility.Visible;
            Logged_Header.Visibility = Visibility.Collapsed;
            MySurveys_BT.Visibility = Visibility.Collapsed;
            if (MainFrame.CanGoBack)
                MainFrame.GoBack();
            MessageBox.Show("Pomyślnie wylogowano", "Wylogowano", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void CreateSurvey_BT_Click(object sender, RoutedEventArgs e)
        {
            if (currentUser == null)
            {
                MessageBoxResult result = MessageBox.Show("Musisz być zalogowany, aby wykonać tę czynność. Przejść do logowania?", "Błąd", MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
                if (result == MessageBoxResult.Yes)
                {
                    LogInWindow logInWindow = new LogInWindow();
                    logInWindow.ShowDialog();
                    if (logInWindow.DialogResult == false)
                        return;
                    else
                    {
                        currentUser = logInWindow.LoggedUser;
                        OnPropertyChanged("UserWelcome");
                        Unlogged_Header.Visibility = Visibility.Collapsed;
                        Logged_Header.Visibility = Visibility.Visible;
                        MySurveys_BT.Visibility = Visibility.Visible;
                    }
                }
                else
                    return;
            }

            if (WarnUser() != MessageBoxResult.Yes)
                return;

            if (createSurveyPage == null)
                createSurveyPage = new CreateSurveyPage();

            createSurveyPage.UpdateCreateSurvey(currentUser);
            MainFrame.Content = createSurveyPage;
        }

        public void MySurveys_BT_Click(object sender, RoutedEventArgs e)
        {
            if (WarnUser() != MessageBoxResult.Yes)
                return;

            ObservableCollection<Survey> surveys = Database.GetDatabase().Query<Survey>("from Survey where User = " + currentUser.ID);
            ObservableCollection<SurveyInterface> surveyInterfaces = new ObservableCollection<SurveyInterface>();

            foreach (Survey survey in surveys)
                surveyInterfaces.Add(new SurveyInterface(survey, PublicSurveysPage.GetQuestionInterfaces(survey.ID)));

            if (mySurveysPage == null)
                mySurveysPage = new MySurveysPage();

            mySurveysPage.UpdateMySurveys(surveyInterfaces);
            MainFrame.Content = mySurveysPage;
            MainFrame_SV.ScrollToHome();
        }

        public MessageBoxResult WarnUser()
        {
            if (MainFrame.Content.GetType() == typeof(CompleteSurveyPage))
                return MessageBox.Show("Opuszczenie ankiety poskutuje utratą odpowiedzi, kontynuować?", "Uwaga", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

            if (MainFrame.Content.GetType() == typeof(CreateSurveyPage))
                return MessageBox.Show("Opuszczenie kreatora poskutuje utratą ankiety, kontynuować?", "Uwaga", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

            return MessageBoxResult.Yes;
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (WarnUser() != MessageBoxResult.Yes)
                e.Cancel = true;
        }
    }
}
