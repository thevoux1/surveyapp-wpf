﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace surveyapp.Models
{
    class EmailSender
    {
        private static EmailSender emailSender;
        private static SmtpClient smtpClient;

        private const string EMAIL_ADDRESS = "noreply.surveyapp@gmail.com";
        private const string EMAIL_PASSWORD = "SecurePassW";
        private const string EMAIL_HOST = "smtp.gmail.com";

        private EmailSender() { }

        public static EmailSender GetEmailSender()
        {
            if (emailSender == null)
            {
                emailSender = new EmailSender();
                smtpClient = new SmtpClient(EMAIL_HOST)
                {
                    Port = 587,
                    Credentials = new NetworkCredential(EMAIL_ADDRESS, EMAIL_PASSWORD),
                    EnableSsl = true,
                };
            }

            return emailSender;
        }

        public async void Send(string recipent, string subject, string body)
        {
            await Task.Run(() =>
            {
                smtpClient.Send(EMAIL_ADDRESS, recipent, subject, body);
            });
        }
    }
}
