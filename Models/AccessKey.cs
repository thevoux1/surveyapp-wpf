﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Models
{
    class AccessKey
    {
        private static readonly Random _random = new Random();

        public static string GetAccessKey()
        {
            ObservableCollection<Survey> surveys = Database.GetDatabase().Query<Survey>("from Survey where AccessKey is not null");
            string key = GenerateKey();

            if (surveys.Count < 1)
                return key;

            foreach(Survey survey in surveys)
            {
                if (survey.AccessKey == key)
                    return GetAccessKey();
            }

            return key;
        }

        public static Survey CheckAccessKey(string accesKey)
        {
            ObservableCollection<Survey> survey = Database.GetDatabase().Query<Survey>("from Survey where AccessKey = '" + accesKey + "'");
            if (survey.Count == 0)
                return null;
            else
                return survey[0];
        }

        private static string GenerateKey()
        {
            StringBuilder passwordBuilder = new StringBuilder();
            passwordBuilder.Append(RandomString(1, true));
            passwordBuilder.Append(RandomString(2));
            passwordBuilder.Append(RandomNumber(11, 99));
            passwordBuilder.Append(RandomString(2, true));
            passwordBuilder.Append(RandomNumber(0, 9));
            passwordBuilder.Append(RandomString(1));
            passwordBuilder.Append(RandomString(1, true));
            passwordBuilder.Append(RandomNumber(11, 99));

            return passwordBuilder.ToString();
        }

        private static string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26;

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        private static int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }
    }
}
