﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using System.Collections.ObjectModel;
using surveyapp.Models;
using System.Threading.Tasks;

namespace surveyapp.Models
{
    class Database
    {
        private static Database database;

        private Configuration myConfiguration;
        private ISessionFactory mySessionFactory;
        private ISession mySession;

        private Database() { }

        public static Database GetDatabase()
        {
            if (database == null)
            {
                database = new Database();
                database.Connect();
            }

            return database;
        }

        public void Connect()
        {
            if (mySession != null && mySession.IsOpen)
            {
                mySession.Close();
            }
            if (mySessionFactory != null && !mySessionFactory.IsClosed)
            {
                mySessionFactory.Close();
            }

            // Inicjowanie NHibernate
            try
            {
                myConfiguration = new Configuration();
                myConfiguration.Configure();
                mySessionFactory = myConfiguration.BuildSessionFactory();
                mySession = mySessionFactory.OpenSession();
                System.Diagnostics.Debug.WriteLine("NHibernate configuration success");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("NHibernate configuration error: " + e.Message);
                throw e;
            }
        }

        public void AddData<Type>(Type obj)
        {
            using (ITransaction transaction = mySession.BeginTransaction())
            {
                mySession.Save(obj);
                transaction.Commit();
            }
        }

        public void AddSurvey(Survey survey, List<Question> questions, List<Answer> answers)
        {
            using (ITransaction transaction = mySession.BeginTransaction())
            {
                mySession.Save(survey);

                foreach(Question question in questions)
                    mySession.Save(question);

                foreach (Answer answer in answers)
                    mySession.Save(answer);

                transaction.Commit();
            }
        }

        public void AddAnswers(List<Answer> answers)
        {
            using (ITransaction transaction = mySession.BeginTransaction())
            {
                foreach (Answer answer in answers)
                    mySession.SaveOrUpdate(answer);

                transaction.Commit();
            }
        }

        public ObservableCollection<Type> Query<Type>(string query)
        {
            return mySession.CreateQuery(query).List<Type>().ToObservableCollection();
        }

        public void UpdateData<Type>(Type obj)
        {
            using (ITransaction transaction = mySession.BeginTransaction())
            {
                try
                {
                    mySession.Update(obj);
                    transaction.Commit();
                }
                catch
                {
                    System.Diagnostics.Debug.WriteLine("Update error");
                    transaction.Rollback();
                }
            }
        }
    }
}
