﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Models
{
    public class Answer
    {
        public Answer() { }
        public virtual int ID { get; set; }
        public virtual Question Question { get; set; }
        public virtual int Selections { get; set; }
        public virtual string Content { get; set; }
    }
}
