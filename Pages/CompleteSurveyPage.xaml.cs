﻿using surveyapp.Interfaces;
using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace surveyapp.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy CompleteSurveyPage.xaml
    /// </summary>
    public partial class CompleteSurveyPage : Page
    {
        private ObservableCollection<QuestionInterface> questions;

        public CompleteSurveyPage(SurveyInterface survey)
        {
            InitializeComponent();
            this.questions = survey.Questions;
            DataContext = survey;
        }

        private void FinishSurvey_BT_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckIfAllAnswered())
            {
                MessageBox.Show("Nie udzielono odpowiedzi na wszystkie pytania!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            List<Answer> answers = new List<Answer>();

            foreach (QuestionInterface question in questions)
            {
                if (question.Question.Type == 0)
                {
                    foreach (AnswerInterface answer in question.Answers)
                    {
                        if (answer.IsChecked == true)
                        {
                            answers.Add(answer.Answer);
                            break;
                        }
                    }
                }
                else if (question.Question.Type == 1)
                {
                    ObservableCollection<Answer> openedAnswers = Database.GetDatabase().Query<Answer>("from Answer where Question = " + question.Question.ID);
                    Answer selectedAnswer = openedAnswers.FirstOrDefault(x => x.Content == question.OpenedAnswer.Trim().ToLower());
                    if (selectedAnswer == null)
                    {
                        selectedAnswer = new Answer()
                        {
                            Question = question.Question,
                            Content = question.OpenedAnswer.Trim().ToLower()
                        };
                    }

                    answers.Add(selectedAnswer);
                }
                else if (question.Question.Type == 2)
                {
                    string textValue;

                    if (question.SliderValue == 0)
                        textValue = "zdecydowanie się nie zgadzam";
                    else if (question.SliderValue == 1)
                        textValue = "nie zgadzam się";
                    else if (question.SliderValue == 2)
                        textValue = "nie wiem";
                    else if (question.SliderValue == 3)
                        textValue = "zgadzam się";
                    else
                        textValue = "zdecydowanie się zgadzam";

                    ObservableCollection<Answer> rangedAnswers = Database.GetDatabase().Query<Answer>("from Answer where Question = " + question.Question.ID);
                    Answer selectedAnswer = rangedAnswers.FirstOrDefault(x => x.Content == textValue);
                    if (selectedAnswer == null)
                    {
                        selectedAnswer = new Answer()
                        {
                            Question = question.Question,
                            Content = textValue
                        };
                    }

                    answers.Add(selectedAnswer);
                }
            }

            foreach (Answer answer in answers)
                answer.Selections++;

            Database.GetDatabase().AddAnswers(answers);
            MainWindow.Instance.MainFrame.GoBack();
            MessageBox.Show("Pomyślnie dodano odpowiedzi", "Dziękujemy", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        private bool CheckIfAllAnswered()
        {
            foreach (QuestionInterface question in questions)
            {
                if (question.Question.Type == 0)
                {
                    bool found = false;
                    foreach (AnswerInterface answer in question.Answers)
                    {
                        if (answer.IsChecked)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                        return false;
                }
                else if (question.Question.Type == 1)
                {
                    if (question.OpenedAnswer == null || question.OpenedAnswer.Trim() == "")
                        return false;
                }
            }

            return true;
        }
    }
}
