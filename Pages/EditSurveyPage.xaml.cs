﻿using surveyapp.Interfaces;
using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace surveyapp.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy EditSurveyPage.xaml
    /// </summary>
    public partial class EditSurveyPage : Page
    {
        private SurveyInterface surveyInterface;

        public EditSurveyPage(SurveyInterface survey)
        {
            InitializeComponent();
            surveyInterface = survey;
            EditSurvey_SP.DataContext = surveyInterface;
            if (surveyInterface.Survey.AccessKey == null)
                IsSurveyPublic_RB.IsChecked = true;
            else
                IsSurveyPrivate_RB.IsChecked = true;
        }

        private void EditSurvey_BT_Click(object sender, RoutedEventArgs e)
        {
            Survey newSurvey = surveyInterface.Survey;

            if (newSurvey.Name == null || newSurvey.Name.Trim() == "")
            {
                MessageBox.Show("Nazwa ankiety nie może być pusta", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.StartDate == null)
            {
                MessageBox.Show("Musisz wybrać datę rozpoczęcia", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.EndDate == null)
            {
                MessageBox.Show("Musisz wybrać datę zakończenia", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.EndDate <= DateTime.Now)
            {
                MessageBox.Show("Data zakończenia musi być większa od daty bieżącej", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.EndDate <= newSurvey.StartDate)
            {
                MessageBox.Show("Data rozpoczęcia musi być mniejsza niż data zakończenia", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.Description == null || newSurvey.Description.Trim() == "")
            {
                MessageBox.Show("Opis ankiety nie może być pusty", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (IsSurveyPrivate_RB.IsChecked == true && newSurvey.AccessKey == null)
                newSurvey.AccessKey = AccessKey.GetAccessKey();
            else if (IsSurveyPrivate_RB.IsChecked == false && newSurvey.AccessKey != null)
                newSurvey.AccessKey = null;

            Database.GetDatabase().UpdateData(surveyInterface.Survey);
            MainWindow.Instance.MainFrame_SV.ScrollToHome();
            MainWindow.Instance.MainFrame.GoBack();
        }
    }
}
