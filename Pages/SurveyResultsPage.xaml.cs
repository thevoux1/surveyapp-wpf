﻿using Microsoft.WindowsAPICodePack.Dialogs;
using surveyapp.Interfaces;
using surveyapp.Models;
using surveyapp.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace surveyapp.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy SurveyResults.xaml
    /// </summary>
    public partial class SurveyResultsPage : Page
    {
        public SurveyResultsPage(SurveyInterface survey)
        {
            InitializeComponent();
            DataContext = survey;
        }

        private void ExportResults_BT_Click(object sender, RoutedEventArgs e)
        {
            ExportResults_BT.Visibility = Visibility.Collapsed;
            ExportResultsWindow exportResultsWindow = new ExportResultsWindow();
            exportResultsWindow.ShowDialog();
            ExportResults_BT.Visibility = Visibility.Visible;
        }
    }
}
