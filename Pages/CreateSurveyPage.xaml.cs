﻿using surveyapp.Models;
using surveyapp.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace surveyapp.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy CreateSurveyPage.xaml
    /// </summary>
    public partial class CreateSurveyPage : Page, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }

        private Survey newSurvey;
        private int questionCounter = 1;

        private ObservableCollection<QuestionInterface> newQuestionsInterfaces;
        public ObservableCollection<QuestionInterface> NewQuestionsInterfaces
        {
            get { return newQuestionsInterfaces; }
            set { newQuestionsInterfaces = value; OnPropertyChanged("NewQuestionsInterfaces"); }
        }

        public CreateSurveyPage()
        {
            InitializeComponent();
            DataContext = this;
        }

        public void UpdateCreateSurvey(User user)
        {
            newSurvey = new Survey()
            {
                User = user
            };
            SurveyProperties_Grid.DataContext = newSurvey;
        }

        private void AddQuestion_BT_Click(object sender, RoutedEventArgs e)
        {
            Question newQuesion = new Question()
            {
                Survey = newSurvey,
                Type = 0
            };

            QuestionInterface newQuestionInterface = new QuestionInterface(newQuesion, null, questionCounter);
            AddQuestionWindow addQuestionWindow = new AddQuestionWindow(newQuestionInterface, false);
            addQuestionWindow.ShowDialog();

            if (addQuestionWindow.DialogResult == true)
            {
                if (NewQuestionsInterfaces == null)
                    NewQuestionsInterfaces = new ObservableCollection<QuestionInterface>();

                NewQuestionsInterfaces.Add(addQuestionWindow.CurrentQuestion);
                questionCounter++;

                if (NoQuestions_LB.Visibility == Visibility.Visible)
                    NoQuestions_LB.Visibility = Visibility.Collapsed;

                MainWindow.Instance.MainFrame_SV.ScrollToEnd();
            }
        }

        private void Remove_Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            QuestionInterface question = (QuestionInterface)(sender as Image).Tag;
            NewQuestionsInterfaces.Remove(question);

            questionCounter = 1;
            foreach (QuestionInterface questioni in NewQuestionsInterfaces)
                questioni.Index = "#" + questionCounter++;

            if (questionCounter == 1)
                NoQuestions_LB.Visibility = Visibility.Visible;
        }

        private void Edit_Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            QuestionInterface question = (QuestionInterface)(sender as Image).Tag;
            AddQuestionWindow addQuestionWindow = new AddQuestionWindow(question, true);
            addQuestionWindow.ShowDialog();

            if (addQuestionWindow.DialogResult == true)
            {
                question = addQuestionWindow.CurrentQuestion;
                question.OnPropertyChanged("Presentation");
                question.OnPropertyChanged("ClosedQuestionVisibility");
                question.OnPropertyChanged("OpenedQuestionVisibility");
                question.OnPropertyChanged("RangedQuestionVisibility");
            }
        }

        private void AddSurvey_BT_Click(object sender, RoutedEventArgs e)
        {
            if (newSurvey.Name == null || newSurvey.Name.Trim() == "")
            {
                MessageBox.Show("Nazwa ankiety nie może być pusta", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.StartDate == null)
            {
                MessageBox.Show("Musisz wybrać datę rozpoczęcia", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.EndDate == null)
            {
                MessageBox.Show("Musisz wybrać datę zakończenia", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.EndDate <= DateTime.Now)
            {
                MessageBox.Show("Data zakończenia musi być większa od daty bieżącej", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.EndDate <= newSurvey.StartDate)
            {
                MessageBox.Show("Data rozpoczęcia musi być mniejsza niż data zakończenia", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (newSurvey.Description == null || newSurvey.Description.Trim() == "")
            {
                MessageBox.Show("Opis ankiety nie może być pusty", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (questionCounter == 1)
            {
                MessageBox.Show("Musisz dodać co najmniej 1 pytanie", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (IsSurveyPrivate_RB.IsChecked == true)
                newSurvey.AccessKey = AccessKey.GetAccessKey();

            List<Question> questions = new List<Question>();
            List<Answer> answers = new List<Answer>();

            foreach (QuestionInterface question in NewQuestionsInterfaces)
            {
                questions.Add(question.Question);
                if (question.Question.Type == 0)
                {
                    foreach (AnswerInterface answer in question.Answers)
                        answers.Add(answer.Answer);
                }
            }

            Database.GetDatabase().AddSurvey(newSurvey, questions, answers);
            MainWindow.Instance.UpdatePublicSurveys();
            MainWindow.Instance.MainFrame.GoBack();

            if (IsSurveyPrivate_RB.IsChecked == true)
            {
                SendPrivateSurveyEmail(newSurvey.User.Email, newSurvey.User.Login, newSurvey.Name, newSurvey.AccessKey);
                MessageBox.Show("Pomyślnie stworzono prywatną ankietę. Kod dostępu został wysłany na Twój adres e-mail.", "Stworzono ankietę", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                SendPublicSurveyEmail(newSurvey.User.Email, newSurvey.User.Login, newSurvey.Name);
                MessageBox.Show("Pomyślnie stworzono publiczną ankietę", "Stworzono ankietę", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void SendPrivateSurveyEmail(string recipent, string userName, string surveyName, string accessKey)
        {
            string subject = "Utworzono prywatną ankietę";
            string body = "Witaj, " + userName + "!\n\nKod dostępu do Twojej ankiety \"" + surveyName + "\" to " + accessKey + "\nPamiętaj, aby przesłać go tylko powołanym osobom!\n\nPozdrawiamy,\nZespół SurveyApp";

            EmailSender.GetEmailSender().Send(recipent, subject, body);
        }

        private void SendPublicSurveyEmail(string recipent, string userName, string surveyName)
        {
            string subject = "Utworzono publiczną ankietę";
            string body = "Witaj, " + userName + "!\n\nTwoja nowa ankieta \"" + surveyName + "\" jest teraz dostępna dla wszystkich\n\nPozdrawiamy,\nZespół SurveyApp";

            EmailSender.GetEmailSender().Send(recipent, subject, body);
        }
    }
}
