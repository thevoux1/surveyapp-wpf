﻿using surveyapp.Interfaces;
using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace surveyapp.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy PublicSurveysPage.xaml
    /// </summary>
    public partial class PublicSurveysPage : Page
    {
        public PublicSurveysPage()
        {
            InitializeComponent();
        }

        public void UpdatePublicSurveys(ObservableCollection<Survey> surveys)
        {
            PublicSurveys_IC.ItemsSource = surveys;
        }

        private void StartSurvey_BT_Click(object sender, RoutedEventArgs e)
        {
            Survey survey = (Survey)(sender as Button).Tag;
            MainWindow.Instance.MainFrame.Content = new CompleteSurveyPage(new SurveyInterface(survey, GetQuestionInterfaces(survey.ID)));
        }
        
        public static ObservableCollection<QuestionInterface> GetQuestionInterfaces(int surveyID)
        {
            ObservableCollection<Question> questions = Database.GetDatabase().Query<Question>("from Question where Survey = " + surveyID);
            ObservableCollection<QuestionInterface> questionInterfaces = new ObservableCollection<QuestionInterface>();

            foreach (Question question in questions)
            {
                int number = questions.IndexOf(question) + 1;
                ObservableCollection<AnswerInterface> answerInterfaces = null;
                ObservableCollection<Answer> answers = Database.GetDatabase().Query<Answer>("from Answer where Question = " + question.ID);
                answerInterfaces = new ObservableCollection<AnswerInterface>();

                foreach (Answer answer in answers)
                {
                    int answerNumber = answers.IndexOf(answer);
                    answerInterfaces.Add(new AnswerInterface(answer, (char)((int)'a' + answerNumber)));
                }

                questionInterfaces.Add(new QuestionInterface(question, answerInterfaces, number));
            }

            return questionInterfaces;
        }
    }
}
