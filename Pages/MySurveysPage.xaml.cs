﻿using surveyapp.Interfaces;
using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace surveyapp.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy MySurveysPage.xaml
    /// </summary>
    public partial class MySurveysPage : Page
    {
        public MySurveysPage()
        {
            InitializeComponent();
        }

        public void UpdateMySurveys(ObservableCollection<SurveyInterface> surveys)
        {
            MySurveys_IC.ItemsSource = surveys;
        }

        private void Remove_Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Podjęcie tego działa trwale usunie ankietę, kontynuować?", "Błąd", MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
            if (result != MessageBoxResult.Yes)
                return;

            SurveyInterface surveyInterface = (SurveyInterface)(sender as Image).Tag;
            ((ObservableCollection<SurveyInterface>)MySurveys_IC.ItemsSource).Remove(surveyInterface);

            Survey survey = surveyInterface.Survey;
            survey.User = null;
            Database.GetDatabase().UpdateData(survey);
        }

        private void Edit_Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SurveyInterface surveyInterface = (SurveyInterface)(sender as Image).Tag;
            EditSurveyPage editSurveyPage = new EditSurveyPage(surveyInterface);
            MainWindow.Instance.MainFrame.Content = editSurveyPage;
            MainWindow.Instance.MainFrame_SV.ScrollToHome();
        }

        private void EndSurvey_BT_Click(object sender, RoutedEventArgs e)
        {
            Survey survey = (Survey)(sender as Button).Tag;
            survey.EndDate = DateTime.Now;
            Database.GetDatabase().UpdateData(survey);
            MainWindow.Instance.MySurveys_BT_Click(sender, e);
            MessageBox.Show("Pomyślnie zakończono ankietę", "Zakończenie ankiety", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void SurveyResults_BT_Click(object sender, RoutedEventArgs e)
        {
            SurveyInterface surveyInterface = (SurveyInterface)(sender as Button).Tag;
            foreach(QuestionInterface questionInterface in surveyInterface.Questions)
                questionInterface.Answers = questionInterface.Answers.OrderByDescending(x => x.Answer.Selections).ToObservableCollection();

            SurveyResultsPage surveyResultsPage = new SurveyResultsPage(surveyInterface);
            MainWindow.Instance.MainFrame.Content = surveyResultsPage;
            MainWindow.Instance.MainFrame_SV.ScrollToHome();
        }
    }
}
